from flask import Flask, jsonify, request
import math

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    result = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        area = abs(math.sin(x_i)) * delta_x
        result += area

    return result

@app.route('/numericalintegralservice', methods=['GET'])
def compute_integral():
    try:
        lower = float(request.args.get('lower'))
        upper = float(request.args.get('upper'))
        N = int(request.args.get('N', default=1000))
        integral_result = numerical_integration(lower, upper, N)
        return jsonify({'result': integral_result})
    except ValueError:
        return jsonify({'error': 'Invalid input. Please provide valid numeric values for lower, upper, and N.'}), 400

if __name__ == '__main__':
    app.run(debug=True)
