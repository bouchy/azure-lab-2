# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

def main(input) -> dict:
    shuffled_data = {}
    for word in input:
        if not shuffled_data.get(word[0]):
            shuffled_data[word[0]]=[]
        shuffled_data[word[0]].append(word[1])
    
    return (shuffled_data)

