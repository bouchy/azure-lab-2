from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)  # Time between requests in seconds

    @task
    def test_microservice(self):
        self.client.get("https://numintegrale.azurewebsites.net/api/numericalintegration")