from locust import HttpUser, task, between

class MyUser(HttpUser):
    wait_time = between(1, 5)  # Time between requests in seconds

    @task
    def test_microservice(self):
        self.client.get("/numericalintegralservice?lower=0&upper=3.14159&N=1000")


