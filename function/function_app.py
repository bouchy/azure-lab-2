import azure.functions as func
import logging
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    result = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        area = abs(math.sin(x_i)) * delta_x
        result += area

    return result

@app.route(route="NumericalIntegration")
def NumericalIntegration(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request for numerical integration.')

    lower = float(req.params.get('lower', '0'))

    upper = float(req.params.get('upper', '3.14159'))
    N = int(req.params.get('N', '1000'))

    integral_result = numerical_integration(lower, upper, N)

    return func.HttpResponse(f"Numerical Integration Result: {integral_result}", status_code=200)